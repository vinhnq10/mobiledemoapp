import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the LocalService provider.
  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
  This is a emulator server to support access data to application.
*/
@Injectable()
export class LocalService {
  fakeData: any;

  constructor(public http: Http) {
    console.log('Init LocalService provider');

    //Get current date
    let now = new Date();
    let nowMilliseconds = now.getTime();
    let milliseconds = 24 * 60 * 60 * 1000;
    //Dummy data
    this.fakeData = {
      users: [
        { email: 'test@gmail.com', pass: '$passw0rd$', name: 'Test' },
        { email: 'qa@gmail.com', pass: '$1$', name: 'QA' },
        { email: 'abc@gmail.com', pass: '$passw0rd$', name: 'Abc' }
      ],
      details: [
        { date_checking: new Date(nowMilliseconds - (3 * milliseconds)), at_location: "ATM withdrawal", amount: 176.22 },
        { date_checking: new Date(nowMilliseconds - (9 * milliseconds)), at_location: "Bill pay", amount: 200 },
        { date_checking: new Date(nowMilliseconds - (27 * milliseconds)), at_location: "Credit card payment", amount: 78.00 },
        { date_checking: new Date(nowMilliseconds - (30 * milliseconds)), at_location: "Bank loan", amount: 23 },
      ],
      checking: 2000
    };
    //Sort DESC data by date_checking
    this.sortPaymentDetailsByDate();
  }

  /**
   * Check user with conditions: Email, Password
   */
  public authUser(email: string, pass: string) {
    if (!email || !pass) return { auth: false, message: 'Please input valid email.', user: null };

    let availUser = this.fakeData.users;
    for (var i = 0; i < availUser.length; i++) {
      if ((email.toLowerCase() === availUser[i].email.toLowerCase()) && (pass === availUser[i].pass)) {
        return { auth: true, message: 'Welcome', user: { email: email, name: availUser[i].name } };
      }
    }

    return { auth: false, message: 'The email address or password you entered is incorrect.', user: null };
  }

  /**
   * Reset password
   */
  public resetPass(email) {
    if (!email) return { reset: false, message: 'Email must not be empty.' };

    let availUser = this.fakeData.users;
    for (var i = 0; i < availUser.length; i++) {
      if (email === availUser[i].email) {
        return { reset: true, message: 'Your password has been reset. Please check your email for more detail.' };
      }
    }

    return { reset: false, message: 'Can not find email address: ' + email };
  }

  /**
   * Get Balance
   */
  public genBankBalance() {
    return { checking: this.fakeData.checking, details: this.fakeData.details };
  }

  /**
   * Make a payment
   */
  public makePayment(reason: any, amount: any) {
    this.fakeData.checking += parseInt(amount);
    this.fakeData.details.push({ date_checking: new Date(), at_location: reason, amount: amount });

    //Sort data by date (desc order)
    this.sortPaymentDetailsByDate();
  }

  public sortPaymentDetailsByDate() {
    this.fakeData.details.sort(function (a, b) { return b.date_checking.getTime() - a.date_checking.getTime() });
  }

}
