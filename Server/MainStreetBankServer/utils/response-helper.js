/* 
 * Copyright Main Street Bank. 2017
 */

/**
 * Response Helper
 * Support set, get to response data
 */

/**
 * Set message
 * @param {type} status
 * @param {type} msg
 * @returns {nm$_response-helper.setMessage.response-helperAnonym$0}
 */
function setMessage(status, msg) {
    return {success: status, msg: msg};
}
function setData(status, data) {
    return {success: status, data: data};
}
function setDataMessage(status, msg, data) {
    return {success: status, msg: msg, data: data};
}

function setFieldMessage(fieldName, msg) {
    return {
        field: fieldName,
        message: msg
    };
}

/**
 * Set data
 * @param {type} status
 * @param {type} data
 * @returns {nm$_response-helper.setData.response-helperAnonym$1}
 */
function setData(status, data) {
    return {success: status, data: data};
}


module.exports.setMessage = setMessage;
module.exports.setData = setData;
module.exports.setDataMessage = setDataMessage;
module.exports.setFieldMessage = setFieldMessage;