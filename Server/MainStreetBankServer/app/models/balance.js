/* 
 * Copyright Main Street Bank. 2017
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;
var User = require('./user');
/**
 * Define Bank for user
 * @type Schema
 */
var BalanceSchema = new Schema({
    amount: {
        required: true,
        type: SchemaTypes.Double
    },
    currency: String,
    user: {
        required: true,
        unique: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: User
    },
    status: Boolean
});

BalanceSchema.pre('save', function (next) {
    var balance = this;
    if (!this.currency) {
        this.currentcy = "USD";
        next();
    }

});

module.exports = mongoose.model('Balance', BalanceSchema);