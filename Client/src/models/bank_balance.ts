export class PaymentDetail{
    public date_payment:Date;
    public pay_reason:string;
    public amount: string;

    constructor(date:Date,reason:string,amount:string){
        this.date_payment = date;
        this.pay_reason = reason;
        this.amount = amount;
    }
}
export class BankBalance {
  public total_checking: string;
  public details: [PaymentDetail];

  constructor(checking: string,detail:[PaymentDetail]) {
    this.total_checking = checking;
    this.details = detail;
  }
}
