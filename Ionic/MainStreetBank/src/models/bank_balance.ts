/**
 * Payment detail model
 */
export class PaymentDetail{
    public date_payment:Date; //Payment date
    public pay_reason:string; 
    public amount: string;  //Amount

    constructor(date:Date,reason:string,amount:string){
        this.date_payment = date;
        this.pay_reason = reason;
        this.amount = amount;
    }
}
/**
 * Bank Balance Model
 */
export class BankBalance {
  public total_checking: string; 
  public details: [PaymentDetail];

  constructor(checking: string,detail:[PaymentDetail]) {
    this.total_checking = checking;
    this.details = detail;
  }
}
