import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { PayeePage } from '../pages/payee/payee';
import { PaymentPage } from '../pages/payment/payment';
import { PaymentResultPage } from '../pages/payment-result/payment-result';

import { EmailValidator } from '../components/email-validator/email-validator';
import { AuthService } from '../providers/auth-service';
import { LocalService } from '../providers/local-service';
import { Storage } from '@ionic/storage';

/**
 * App has a root module that essentially controls the rest of the application
 * Link: https://ionicframework.com/docs/v2/intro/tutorial/project-structure/
 */
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    EmailValidator,
    PayeePage,
    PaymentPage,
    PaymentResultPage
  ],
  imports: [
    IonicModule.forRoot(MyApp,{backButtonText: ' '})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    PayeePage,
    PaymentPage,
    PaymentResultPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},AuthService,LocalService,Storage]
})
export class AppModule {}
