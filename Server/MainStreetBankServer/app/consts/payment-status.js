/* 
 * Copyright Main Street Bank. 2017
 */

var PaymentStatus = {
    Completed: "Completed", //: The payment has been completed, and the funds have been added successfully to your account balance.
    Created: "Created", //: A German ELV payment is made using Express Checkout.
    Denied: "Denied", //: You denied the payment. This happens only if the payment was previously pending because of possible reasons described for the pending_reason variable or the Fraud_Management_Filters_x variable.
    Failed: "Failed", //: The payment has failed. This happens only if the payment was made from your customer’s bank account.
    Pending: "Pending", //: The payment is pending. See pending_reason for more information.
    Refunded: "Refunded", //You refunded the payment.
    Reversed: "Reversed", // A payment was reversed due to a chargeback or other type of reversal. The funds have been removed from your account balance and returned to the buyer. The reason for the reversal is specified in the ReasonCode element.
    Processed: "Processed", // A payment has been accepted.
    Voided: "Voided"//This authorization has been voided.
};


module.exports.CONST_PaymentStatus = PaymentStatus;