
/* 
 * Copyright Main Street Bank. 2017
 */

/* global Promise, regexCurrency */

//Setup
var appName = 'Main Street Bank';
var express = require('express'); ////express is the popular Node framework
var app = express();
var bodyParser = require('body-parser'); //body-parser will let us get parameters from our POST requests
var morgan = require('morgan'); //morgan will log requests to the console so we can see what is happening
var mongoose = require('mongoose'); //mongoose is how we interact with our MongoDB database
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var passport = require('passport');
var jwt = require('jwt-simple'); //jsonwebtoken is how we create and verify our JSON Web Tokens
var cors = require('cors'); //CORS is a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options.

var config = require('./config/database'); //Get db config file
var responseHelper = require('./utils/response-helper');
var tokenHelper = require('./utils/token-helper');
var validateHelper = require('./utils/validate-helper');

var User = require('./app/models/user'); // get the mongoose model
var Payee = require('./app/models/payee');
var Balance = require('./app/models/balance');
var Payment = require('./app/models/payment');
var regexCurrency = /^[1-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;

var port = process.env.PORT || 6789;

//Configuration
//mongoose.Promise = require('bluebird');
//mongoose.Promise = Promise;
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended': 'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({type: 'application/vnd.api+json'})); // parse application/vnd.api+json as json
app.use(methodOverride());
app.use(cors());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Use the passport package in our application
app.use(passport.initialize());

// demo Route (GET http://localhost:8080)
app.get('/', function (req, res) {
    res.send('Hello! ' + appName + ' API is at http://localhost:' + port + '/api');
});


//Connect to database
mongoose.connect(config.database);

//pass passport for configuration
require('./config/passport')(passport);

//Init data
initData();

// bundle our routes
var apiRoutes = express.Router();

// USER API
// create a new user account (POST http://localhost:8080/api/signup)
apiRoutes.post('/signup', function (req, res) {
    console.log(req.body);
    if (!req.body.username || !req.body.password) {
        res.json(responseHelper.setMessage(false, 'Please fill out the complete form.'));
    } else {
        var newUser = new User({
            username: req.body.username,
            password: req.body.password
        });

        // save the user
        newUser.save(function (err) {
            if (err) {
                console.log(err);
                return res.json(responseHelper.setMessage(false, 'Username already exists.'));
            }
            //console.log(newUser);
            //Set default Balance
            var balance = new Balance({
                amount: 2000,
                user: newUser._id,
                status: true
            });
            balance.save(function (err) {
                if (err) {
                    console.log("Set default Balance failed.");
                } else {
                    console.log("Success created Balnace for user.");
                }
            });

            res.json(responseHelper.setMessage(true, 'Successful created new user.'));
        });
    }
});

// route to authenticate a user (POST http://localhost:8080/api/authenticate)
apiRoutes.post('/authenticate', function (req, res) {
    var username = req.body.username;
    console.log('Username: ' + username);
    User.findOne({
        username: username
    }, function (err, user) {
        if (err)
            throw err;

        if (!user) {
            res.send(responseHelper.setMessage(false, 'Authentication failed. User not found.'));
        } else {
            //Check if password matchs
            user.comparePassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                    user.last_login_at = new Date();
                    user.save(function (err) {
                        if (err) {
                            console.log("Update date last login failed.");
                        } else {
                            console.log("Successful updated user.");
                        }
                    });
                    console.log(user);
                    // if user is found and password is right create a token
                    var token = jwt.encode(user, config.secret);
                    //Return token and user login infomation
                    res.json({
                        success: true,
                        username: username,
                        token: 'JWT ' + token
                    });
                } else {
                    res.send(responseHelper.setMessage(false, 'Authentication failed. Wrong password.'));
                }
            });
        }
    });
});

apiRoutes.get('/memberinfo', passport.authenticate('jwt', {session: false}), function (req, res) {
    console.log(req.headers);
    var token = tokenHelper.getToken(req.headers);
    if (token) {
        var decode = jwt.decode(token, config.secret);
        User.findOne({
            username: decode.username
        }, function (err, user) {
            if (err)
                throw err;

            if (!user) {
                res.status(403).send(responseHelper.setMessage(false, 'Authentication failed. User not found.'));
            } else {

                res.json(responseHelper.setData(true, {
                    username: user.username,
                    created_at: user.created_at
                }));
            }
        });
    } else {
        res.status(403).send(responseHelper.setMessage(false, 'No token provided'));
    }
});

//PAYEE API
apiRoutes.get('/payee', passport.authenticate('jwt', {session: false}), function (req, res) {
    var token = tokenHelper.getToken(req.headers);
    if (token) {
        var decode = jwt.decode(token, config.secret);
        User.findOne({
            username: decode.username
        }, function (err, user) {
            if (err)
                throw err;

            if (!user) {
                res.status(403).send(responseHelper.setMessage(false, 'Authentication failed. User not found.'));
            } else {

                Payee.find(function (err, payee_list) {
                    if (err)
                        throw err;

                    if (!payee_list) {
                        return res.status(403).send({success: false, msg: 'No Payee Found.'});
                    } else {
                        res.json(responseHelper.setData(true, payee_list));
                    }
                });
            }
        });
    } else {
        res.status(403).send(responseHelper.setMessage(false, 'No token provided'));
    }
});
apiRoutes.get('/get-payee', function (req, res) {
    Payee.find(function (err, payee_list) {
        if (err)
            throw err;

        if (!payee_list) {
            return res.status(403).send({success: false, msg: 'No Payee Found.'});
        } else {
            res.json(responseHelper.setData(true, payee_list));
        }
    });
});


apiRoutes.get('/balance', passport.authenticate('jwt', {session: false}), function (req, res) {
    var token = tokenHelper.getToken(req.headers);
    if (token) {
        var userLogin = jwt.decode(token, config.secret);
        Balance.findOne({
            user: userLogin._id
        }, function (err, balance) {
            if (err)
                throw err;

            if (!balance) {
                res.status(403).send(responseHelper.setMessage(false, 'Authentication failed. Balance not found.'));
            } else {
                //delete balance.user;
                var data = {
                    balance: balance,
                    payments: []
                };
                //Get recent activity
                Payment.find({
                    payer: userLogin._id
                }, function (err, payments) {
                    if (!err) {
                        data.payments = payments;
                        res.json(responseHelper.setData(true, data));
                    } else {
                        res.json(responseHelper.setData(true, data));
                    }
                });
            }
        });
    } else {
        res.status(403).send(responseHelper.setMessage(false, 'No token provided'));
    }
});

apiRoutes.post('/make-a-payment', passport.authenticate('jwt', {session: false}), function (req, res) {
    var token = tokenHelper.getToken(req.headers);
    if (token) {
        var errorInput = [];
        var dataPost = req.body;
        if (typeof dataPost.payee === "undefined") {
            errorInput.push(responseHelper.setFieldMessage("payee", "Payee required"));
        }
        if (typeof dataPost.amount === "undefined") {
            errorInput.push(responseHelper.setFieldMessage("amount", "Amount required"));
        }
        if (errorInput.length === 0) {
            var payer = jwt.decode(token, config.secret);
            Balance.findOne({
                user: payer._id
            }, function (err, balance) {
                if (err)
                    throw err;
                //Check Balance
                if (!balance) {
                    res.status(403).send(responseHelper.setMessage(false, 'Authentication failed. Balance not found.'));
                } else {
                    //Check payee
                    Payee.findOne({_id: dataPost.payee}, function (err, payee) {
                        if (err || payee === null) {
                            res.status(403).send(responseHelper.setMessage(false, 'Payee not found.'));
                        } else {
                            if (regexCurrency.test(dataPost.amount)) {
                                var amount = balance.amount;
                                //Check Balance amount
                                /*if (amount >= dataPost.amount) {*/
                                    console.log("dataPost.amount: " + dataPost.amount);
                                    var payment = new Payment({
                                        payee: payee._id,
                                        amount: dataPost.amount,
                                        description: payee.description,
                                        payer: payer._id
                                    });
                                    //Create new payment
                                    payment.save(function (err, newPayment) {
                                        if (err) {
                                            res.status(403).send(responseHelper.setDataMessage(false, "Payment transaction don't success. Please try again.", validateHelper.mongooseErrors(err)));
                                        } else {
                                            //Update Balance Amount
                                            balance.amount = balance.amount - dataPost.amount;
                                            balance.save(function (err, newBalanceUpdate) {
                                                //Error
                                                if (err) {
                                                    //Remove payment
                                                    payment.remove({"_id": newPayment._id}, function (err, result) {
                                                        if (result === 1) {
                                                            console.log("payment Deleted");
                                                        } else {
                                                            console.log('error: ' + err);
                                                        }
                                                    });
                                                    res.status(403).send(responseHelper.setMessage(false, "Payment transaction don't success. Please try again."));
                                                } else {
                                                    res.json(responseHelper.setData(true, "The payment has been successful."));
                                                }
                                            });
                                        }
                                    });
                               /* } else {
                                    res.status(403).send(responseHelper.setMessage(false, "Curent account is $" + balance.amount + " not enough to pay with $" + dataPost.amount + ". Please try check again."));
                                }*/
                            } else {
                                res.json(responseHelper.setData(true, "Amount invalid."));
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).send(responseHelper.setDataMessage(false, "Data input invaild.", errorInput));
        }
    } else {
        res.status(403).send(responseHelper.setMessage(false, 'No token provided'));
    }
});



function initData() {
    //Check Payee Collection
    Payee.find(function (err, payee_list) {
        if (!err && (payee_list.length === 0)) {
            var payeeList = [
                {
                    name: "Credit Card",
                    description: "Credit Card payment"
                },
                {
                    name: "Bank Loan",
                    description: "Bank Loan payment"
                },
                {
                    name: "Utility",
                    description: "Bill pay"
                }
            ];
            savePayees(payeeList);
        }
    });
}

function savePayees(payees) {
    Payee.collection.insert(payees, function callback(error, insertedDocs) {
        console.log(insertedDocs);
    });
}

//Start server
app.listen(port);
console.log(appName + " API listening on port " + port);

// connect the api routes under /api/*
app.use('/api', apiRoutes);












