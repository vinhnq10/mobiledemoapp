import { Component } from '@angular/core';
import { MenuController } from 'ionic-angular';
import { NavController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { HomePage } from '../home/home';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on Ionic pages and navigation.
*/

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {
  loading: Loading;
  registerCredentials = { email: '', password: '' };

  constructor(public nav: NavController, public menu: MenuController, private auth: AuthService, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    menu.swipeEnable(false);
  }

  public forgotPassword() {
    this.showAlertForgot();
  }

  public login() {
    this.showLoading()
    this.auth.login(this.registerCredentials).subscribe(allowed => {
      if (allowed.auth) {
        setTimeout(() => {
          this.loading.dismiss();
          this.nav.setRoot(HomePage)
        });
      } else {
        this.showError(allowed.message);
      }
    },
      error => {
        this.showError(error);
      });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  showError(text) {
    setTimeout(() => {
      try {
        this.loading.dismiss();
      }
      catch (error) {
        console.log('Cannot dismiss the loading. Error detail: ' + error);
      }
    });

    let alert = this.alertCtrl.create({
      title: 'Invalid',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  resetPass(email: string) {
    let alert = this.alertCtrl.create({
      title: 'Reset Password',
      subTitle: 'Please check your mail to reset your password',
      buttons: ['OK']
    });
    this.showLoading();
    this.auth.resetPassword(email).subscribe(result => {
      this.loading.dismiss().then(() => {
        if (result.reset) {
          alert.present(prompt);
        } else {
          this.showError(result.message);
        }
      });
    },
      error => {
        this.showError(error);
      })
  }

  showAlertForgot() {
    let alert = this.alertCtrl.create({
      title: 'Forgot Password',
      inputs: [
        {
          name: 'email',
          placeholder: 'Enter your email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Submit',
          handler: data => {
            let navTransition = alert.dismiss();
            if (data.email) {
              navTransition.then(() => {
                this.resetPass(data.email);
              });
              return false;
            } else {
              navTransition.then(() => {
                this.showError("Please input valid email"); // + data.email);
              });
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }
}