import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the PaymentResult page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-payment-result',
  templateUrl: 'payment-result.html'
})
export class PaymentResultPage {
  payType:any;
  amoun:any;
  date:any
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.payType = navParams.data.type;
    this.amoun = navParams.data.pay;
    this.date = new Date();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentResultPage');
  }

  public backToPaynee(){
    this.navCtrl.popTo(this.navCtrl.indexOf(this.navCtrl.getPrevious())-1);
  }

}
