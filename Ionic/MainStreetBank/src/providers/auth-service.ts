import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { User } from '../models/user';
import { LocalService } from './local-service';
import { Storage } from '@ionic/storage';

const USER_KEY = "user";
/**
 * Auth Service
 * The service support functions:
 * - login()          : Perform log in
 * - getUserInfo()    : Get user info
 * - logout()         : Log out
 * - resetPassword()  : Reset password
 * - getBankBalance() : Get balance
 * - makePayment()    : Make a payment
 */
@Injectable()
export class AuthService {
  constructor(public server: LocalService, public storage: Storage) {
    console.log('Init AuthService: Inject the LocalService provider and Storage object');
  }

  /**
   * Log in function
   */
  public login(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please input valid credentials.");
    } else {
      return Observable.create(observer => {
        // At this point make a request to your backend to make a real check!
        setTimeout(() => {
          let access = this.server.authUser(credentials.email, credentials.password);
          //Save user credentials
          if (access.auth) {
            this.storage.set(USER_KEY, JSON.stringify(access.user));
          }
          observer.next(access);
          observer.complete();
        }, 2000);
      });
    }
  }

  /**
   * Get user info
   */
  public getUserInfo(): any {
    var user: any;
    //Get user from local storage
    this.storage.get(USER_KEY).then((data) => {
      if (data != null) {
        user = JSON.parse(data);
        return new User(user.email, user.name)
      } else {
        return null;
      }
    });
  }

  /**
   * Log out
   */
  public logout() {
    return Observable.create(observer => {
      //Remove USER_KEY out of local storage
      this.storage.remove(USER_KEY);
      setTimeout(() => {
        observer.next(true);
        observer.complete();
      }, 2000);
    });
  }

  /**
   * Reset password
   */
  public resetPassword(email: string) {
    return Observable.create(observer => {
      setTimeout(() => {
        //Call a service to reset password. Set latency of 2secs to simulate the executing requests in server side (API calls)
        let result = this.server.resetPass(email);
        observer.next(result);
        observer.complete();
      }, 2000);
    });
  }

  /**
   * Fetch balance from user's bank account
   */
  public getBankBalance() {
    return Observable.create(observer => {
      //Set latency of 2secs to simulate the executing requests in server side (API calls)
      setTimeout(() => {
        //Fetch result from data service
        let result = this.server.genBankBalance();
        observer.next(result);
        observer.complete();
      }, 2000);
    });
  }

  /**
   * Make a payment
   */
  public makePayment(reason: any, amount: any) {
    return Observable.create(observer => {
      //Set latency of 2secs 
      setTimeout(() => {
        //Call service to process a payment
        this.server.makePayment(reason, amount);
        observer.next(true);
        observer.complete();
      }, 2000);
    });
  }
}