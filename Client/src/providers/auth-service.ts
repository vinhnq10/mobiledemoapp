import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { User } from '../models/user';
import { LocalService } from './local-service'
import { Storage } from '@ionic/storage';

const SERVER_URL = "http://localhost:8899/api/";
const USER_TOKEN_KEY = "user_token";
const USER_KEY = "user";
@Injectable()
export class AuthService {
  token: any;
  constructor(public server: LocalService,
    public storage: Storage,
    public http: Http) {
    console.log('Hello LocalService Provider');
  }

  public login(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      return Observable.create(observer => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post(SERVER_URL + "authenticate", JSON.stringify({
          username: credentials.email,
          password: credentials.password
        }), { headers: headers })
          .map(res => res.json())
          .subscribe(dataRes => {
            console.log(dataRes);
            if (dataRes.success) {
              this.storage.set(USER_KEY, JSON.stringify(dataRes));
              this.storage.set(USER_TOKEN_KEY, JSON.stringify(dataRes.token));
            }
            observer.next(dataRes);
            observer.complete();
          });
        /*
        // At this point make a request to your backend to make a real check!
        setTimeout(() => {
          // let access = (credentials.password === "pass" && credentials.email === "email");
          let access = this.server.authUser(credentials.email, credentials.password);
         
          //storage User
          if(access.auth){
            this.storage.set(USER_KEY,JSON.stringify(access.user));
          }
          // this.currentUser = new User('Simon', credentials.email);
          observer.next(access);
          observer.complete();
        }, 2000);
        */
      });
    }
  }

  public getUserInfo(): any {
    var user: any;
    this.storage.get(USER_KEY).then((data) => {
      if (data != null) {
        user = JSON.parse(data);
        return new User(user.email, user.name)
      } else {
        return null;
      }
    });
  }

  public logout() {
    return Observable.create(observer => {
      this.storage.remove(USER_KEY);
      setTimeout(() => {
        observer.next(true);
        observer.complete();
      }, 2000);
    });
  }

  public resetPassword(email: string) {
    return Observable.create(observer => {
      setTimeout(() => {
        let result = this.server.resetPass(email);
        observer.next(result);
        observer.complete();
      }, 2000);
    });
  }

  public getBankBalance() {
    return Observable.create(observer => {
      //setTimeout(() => {
        this.token = this.storage.get(USER_TOKEN_KEY);
        console.log(this.token);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.token = "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1OGFmZTNiNmYzYzJkNTFmMWM5MDlkMDQiLCJjcmVhdGVkX2F0IjoiMjAxNy0wMi0yNFQwNzo0MTo0Mi4zMDNaIiwidXNlcm5hbWUiOiJkZW1vQGV2aXppLmNvbSIsInBhc3N3b3JkIjoiJDJhJDEwJERFNTJGQXRBeGR1ZE1hYzZLSzNwTU90Z3RsVTRIS1Y1OHF1UThtRkFXRGJVUXdhSTcvNTltIiwiX192IjowLCJsYXN0X2xvZ2luX2F0IjoiMjAxNy0wMi0yNFQwODo0NTo1OC40OTNaIn0.wRoKU7uNqRtFRilinuoCfBusI9QAoWHbyk8Zf4fR8_U";
        headers.append('Authorization', this.token);
        let result = [];
        this.http.get(SERVER_URL + "balance", { headers: headers })
          .map(res => res.json())
          .subscribe(dataRes => {
            console.log(dataRes);
            if (dataRes.success) {
              result = dataRes.data;
            }
            observer.next(result);
            observer.complete();
          });

      //}, 2000);
    });
  }

  public makePayment(reason: any, amount: any) {
    return Observable.create(observer => {
      setTimeout(() => {
        this.server.makePayment(reason, amount);
        observer.next(true);
        observer.complete();
      }, 2000);
    });
  }
}