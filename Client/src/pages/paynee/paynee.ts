import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PaymentPage } from '../payment/payment';

/*
  Generated class for the Paynee page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-paynee',
  templateUrl: 'paynee.html'
})
export class PayneePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad PayneePage');
  }

  public cancle(){
    this.navCtrl.pop();
  }

  public creditPayment(){
    this.navCtrl.push(PaymentPage,{type:'Credit Card'});
  }

  public loanPayment(){
    this.navCtrl.push(PaymentPage,{type:'Bank Loan'});
  }

  public utilityPayment(){
    this.navCtrl.push(PaymentPage,{type:'Utility'});
  }

}
