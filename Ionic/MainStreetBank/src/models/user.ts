/**
 * User model
 */
export class User {
  public name: string;
  public email: string;

  constructor(email: string,name:string) {
    this.name = name;
    this.email = email;
  }
}