/* 
 * Copyright Main Street Bank. 2017
 */
/**
 * Validate helper
 * Support get validate data
 */
var responseHelper = require('./response-helper');
/**
 * validateHelper
 * @param {type} errors
 * @returns {nm$_validate-helper.module.exports.validateHelper.parted}
 */
module.exports.mongooseErrors = function (errors) {
    var msgArray = [];
    if (errors.errors) { // validation errors
        msgArray.push(responseHelper.setFieldMessage(
                errors.errors.amount.path,
                errors.errors.amount.message));
    } else if (errors.message) { // should be execution error without err.errors
        console.log(errors); // log execution errors
        msgArray.push(errors.message);
    } else {
        msgArray.push('Unknown error');
    }
    return msgArray;
};