import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { PaymentResultPage } from '../payment-result/payment-result';
import { AuthService } from '../../providers/auth-service';
import { HomePage } from '../home/home';

const regexCurrency = /^[1-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;

/*
  Generated class for the Payment page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html'
})
export class PaymentPage {
  paymentType: any;
  money: any;
  loading: Loading;

  constructor(public navCtrl: NavController, public auth: AuthService, private alertCtrl: AlertController, private loadingCtrl: LoadingController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController) {
    this.paymentType = navParams.data.type;
    // this.money = 0;
  }

  //ionViewDidLoad fires if the page is not cached
  ionViewDidLoad() {
    console.log('PaymentPage: The event ionViewDidLoad fires');
  }

  showPaynee() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Choose a payee',
      buttons: [
        {
          text: 'Credit Card',
          handler: () => {
            this.paymentType = 'Credit Card';
          }
        },
        {
          text: 'Bank Loan',
          handler: () => {
            this.paymentType = 'Bank Loan';
          }
        }, {
          text: 'Utility',
          handler: () => {
            this.paymentType = 'Utility';
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  public makePayment() {
    this.showLoading();
    let alert = this.alertCtrl.create({
      title: 'Invalid',
      subTitle: 'Please fill a valid amount',
      buttons: ['OK']
    });
    var validAmount = regexCurrency;
    if (validAmount.test(this.money)) {
      this.auth.makePayment(this.paymentType, this.money).subscribe((result) => {
        if (result) {
          this.navCtrl.push(PaymentResultPage, { type: this.paymentType, pay: this.money });
        }
        this.loading.dismiss();
      });
    } else {
      this.loading.dismiss();
      alert.present();
    }
  }

  public cancelPayment() {
    this.navCtrl.push(HomePage, {});
  }
}
