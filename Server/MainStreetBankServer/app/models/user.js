/* 
 * Copyright Main Street Bank. 2017
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var validator = require('node-mongoose-validator');

var validateEmail = function (email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
};


// set up User mongoose model
var UserSchema = new Schema({
    username: {
        /*type: String,
         unique: true,
         required: [true, "Username required"],
         trim: true,
         validate: {
         validator: function(v) {
         return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v);
         },
         message: '{VALUE} is not a valid email!'
         }*/
        type: String,
        trim: true,
        lowercase: true,
        unique: true
                //validate: [validateEmail, 'Please fill a valid email address1'],
                //match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']

    },
    password: {
        type: String,
        required: true
    },
    created_at: Date,
    last_login_at: Date
});

// Validations
// See https://github.com/SamVerschueren/node-mongoose-validator
UserSchema.path('username').validate(validator.notEmpty(), 'Email address is required.');
UserSchema.path('username').validate(validator.isEmail(), 'Please provide a valid email address');


UserSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        // get the current date
        var currentDate = new Date();
        // if created_at doesn't exist, add to that field
        if (!this.created_at) {
            this.created_at = currentDate;
        }
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', UserSchema);