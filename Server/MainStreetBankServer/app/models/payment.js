/* 
 * Copyright Main Street Bank. 2017
 */

var mongoose = require('mongoose');
var Scheme = mongoose.Schema;
require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;
var validator = require('node-mongoose-validator');
var User = require('./user');
var Payee = require('./payee');

//http://stackoverflow.com/questions/2227370/currency-validation
var regexCurrency = /^[1-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
var validateAmount = function (value) {
    var re = regexCurrency;
    console.log("re.test(value)");
    console.log(re.test(value));
    return re.test(value);
};
var PaymentSchema = new Scheme({
    payee: {
        required: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: Payee
    },
    description: String,
    amount: {
        required: [true, "Amount required"],
        type: SchemaTypes.Double,
        //validate: [validateAmount, 'Please fill a valid amount']
    },
    currency: {
        type: String,
        default: "USD"
    },
    payer: {
        required: [true, "Payer required"],
        type: mongoose.Schema.Types.ObjectId,
        ref: User
    },
    status: Boolean,
    payment_date: {
        type: Date,
        default: Date.now
    }
});

//Validate
PaymentSchema.path('payee').validate(validator.isMongoId(), 'Payee id invalid');
PaymentSchema.path('payer').validate(validator.isMongoId(), 'Payer id invalid');
//PaymentSchema.path('amount').validate(validator.matches(regexCurrency), 'Amount invalid');


PaymentSchema.pre('save', function (next) {
    var self = this;
    var foundUpdate = false;
    if (!this.currency) {
        this.currentcy = "USD";
        foundUpdate = true;
    }
    if (!this.status) {
        this.status = true;
        foundUpdate = true;
    }
    console.log("this:");
    console.log(this);
    if (foundUpdate) {
        console.log("Has update on Payment Schema");
        next();
    }
});


module.exports = mongoose.model('Payment', PaymentSchema);