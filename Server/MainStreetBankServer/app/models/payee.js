/* 
 * Copyright Main Street Bank. 2017
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Setup Payee mongoDB model
var PayeeSchema = new Schema({
    name: String,
    description: String
});

module.exports = mongoose.model('Payee', PayeeSchema);