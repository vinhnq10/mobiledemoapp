Document:

1. First you gotta edit the mongoDB database string in /config/database.js

2. Start server
    node server.js
    consolog: Main Street Bank Application listening on port 8899

    if node_modules don't install. We can run repeat with : 
        npm install <package> 
        node server.js
======================================
Call API
--------------------------------------
SIGN UP:
    Content-Type : application/x-www-form-urlencoded
    POST /api/signup
        username
        password
--------------------------------------
LOGIN:
    POST /api/authenticate
        username
        password
--------------------------------------
GET MEMBER INFO:
    GET /api/memberinfo
        Authorization : JWT key
--------------------------------------
GET PAYEE:
    Request:
        GET /api/payee
        Authorization : JWT key
    Response:
        status: true,
        data : [
            {
                _id: ...
                name: ...,
                description: ...
            }
        ]

--------------------------------------
GET BALANCE:
    Request:
        GET /api/balance
        Authorization : JWT key
    Response:
        {
            "success": true,
            "data": {
                "balance": {
                    "_id": "58ac156dabe1982b185a945b",
                    "amount": 1946,
                    "user": "58ac156dabe1982b185a945a",
                    "status": true,
                    "__v": 0
                },
                "payments": [
                    {
                      "_id": "58ad60dc6d450d8ed3f59581",
                      "payment_date": "2017-02-22T09:58:52.008Z",
                      "status": true,
                      "payee": "58ac22da2a466d7649128442",
                      "amount": 1000,
                      "description": "Credit Card payment",
                      "payer": "58ac156dabe1982b185a945a",
                      "__v": 0
                }]
            }
        }

--------------------------------------
MAKE A PAYMENT
    Method : POST
    Request:
        GET /api/balance
        Authorization : JWT key
        Content-Type : application/x-www-form-urlencoded
        body : {
            payee: payee Id (See id on API : /api/payee),
            amount : number
        }
    Response:
        {
            "success": true,
            "data": "The payment has been successful."
        }










RESEARCH & :
API:
https://www.mongodb.com/blog/post/building-your-first-application-mongodb-creating-rest-api-using-mean-stack-part-1

The CRUD acronym is often used to describe database operations. CRUD stands for CREATE, READ, UPDATE, and DELETE. These database operations map very nicely to the HTTP verbs, as follows:

    POST: A client wants to insert or create an object.
    GET: A client wants to read an object.
    PUT: A client wants to update an object.
    DELETE: A client wants to delete an object.

These operations will become clear later when define our API.

Some of the common HTTP result codes that are often used inside REST APIs are as follows:

    200 - “OK”.
    201 - “Created” (Used with POST).
    400 - “Bad Request” (Perhaps missing required parameters).
    401 - “Unauthorized” (Missing authentication parameters).
    403 - “Forbidden” (You were authenticated but lacking required privileges).
    404 - “Not Found”.

-----------
Currency
    https://github.com/mongoosejs/mongoose-double
-----------
Validate:
    https://github.com/leepowellcouk/mongoose-validator
    https://github.com/SamVerschueren/node-mongoose-validator

ISSUES :

    'cros' is not in the npm registry.
        npm install browser-sync gulp --save-dev