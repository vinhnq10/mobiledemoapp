/* 
 * Copyright Main Street Bank. 2017
 */

/**
 * Database
 * Config data
 */

module.exports = {
    'secret': 'MainStreetBank@2017',
    'database': 'mongodb://localhost/MainStreetBank'
};
