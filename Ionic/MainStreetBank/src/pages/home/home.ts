import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { PayeePage } from '../payee/payee';
import { LoginPage } from '../login/login';


/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on Ionic pages and navigation.
*/

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  bank_balance: any;
  checking: any;
  loading: Loading;
  recentActivityMgs = "Loading recent activity...";
  constructor(public nav: NavController, private loadingCtrl: LoadingController, public auth: AuthService,
    public navParams: NavParams, menu: MenuController) {
    menu.swipeEnable(true);
  }

  ionViewWillEnter() {
    if (!this.checking) {
      this.showLoading();
    }
    this.auth.getBankBalance().subscribe((result) => {
      if (!this.checking) {
        this.loading.dismiss();
      }
      this.checking = result.checking;
      if (result.details.length <= 0) {
        this.recentActivityMgs = "No recent activity to display";
      }
      this.bank_balance = result.details;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  public logout() {
    this.showLoading();
    this.auth.logout().subscribe(() => {
      this.loading.dismiss();
      this.nav.setRoot(LoginPage);
    });
  }

  public makePayment() {
    this.nav.push(PayeePage);
  }
}