import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PaymentPage } from '../payment/payment';

/*
  Generated class for the Payee page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on Ionic pages and navigation.
*/

@Component({
  selector: 'page-payee',
  templateUrl: 'payee.html'
})

export class PayeePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  //ionViewDidLoad just fires if the page is not cached
  ionViewDidLoad() {
    console.log('PayeePage: The event ionViewDidLoad fires');
  }

  public cancel(){
    this.navCtrl.pop();
  }

  public creditPayment(){
    this.navCtrl.push(PaymentPage,{type:'Credit Card'});
  }

  public loanPayment(){
    this.navCtrl.push(PaymentPage,{type:'Bank Loan'});
  }

  public utilityPayment(){
    this.navCtrl.push(PaymentPage,{type:'Utility'});
  }
}