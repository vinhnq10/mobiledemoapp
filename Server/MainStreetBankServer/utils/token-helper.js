/* 
 * Copyright Main Street Bank. 2017
 */
/**
 * Token helper
 * Support get token data
 */

/**
 * Get token form header
 * @param {type} headers
 * @returns {nm$_token-helper.module.exports.getToken.parted}
 */
module.exports.getToken = function (headers) {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};