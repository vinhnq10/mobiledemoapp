import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the LocalService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LocalService {
  fakeData: any;

  constructor(public http: Http) {
    console.log('Hello LocalService Provider');
    let now = new Date();
    this.fakeData = {
      users: [
        { email: 'test@gmail.com', pass: '123', name: 'test' },
        { email: 'abc@gmail.com', pass: '123', name: 'abc' }
      ],
      details: [
        { date_checking: new Date(now.getMilliseconds() - 10000), at_location: "ATM withdrawal", amount: 1000 },
        { date_checking: new Date(now.getMilliseconds() - 20000), at_location: "Bill pay", amount: 2300 },
        { date_checking: new Date(now.getMilliseconds() - 30000), at_location: "Credit card payment", amount: 2300 },
        { date_checking: new Date(now.getMilliseconds() - 140000), at_location: "Bank loan", amount: 2300 },
      ],
      checking:2000
    };
  }

  public authUser(email: string, pass: string) {
    if (!email || !pass) return { auth: false, message: 'Email and password must not empty', user: null };
    console.log(JSON.stringify(this.fakeData));
    let availUser = this.fakeData.users;
    for (var i = 0; i < availUser.length; i++) {
      if (email === availUser[i].email && pass === availUser[i].pass) {
        return { auth: true, message: 'Welcome', user: { email: email, name: availUser[i].name } };
      }
    }
    return { auth: false, message: 'Your email or password not match', user: null };
  }

  public resetPass(email) {
    if (!email) return { reset: false, message: 'Email must not empty' };
    let availUser = this.fakeData.users;
    for (var i = 0; i < availUser.length; i++) {
      if (email === availUser[i].email) {
        return { reset: true, message: 'Your password has reset, please check your email for more detail' };
      }
    }
    return { reset: false, message: 'Can not find email: ' + email };
  }

  public genBankBalance() {
    return { checking: this.fakeData.checking, details: this.fakeData.details };
  }

  public makePayment(reason:any,amount:any){
    this.fakeData.checking +=parseInt(amount);
    this.fakeData.details.push({ date_checking: new Date(), at_location: reason, amount: amount });
  }

}
